package ru.tsc.felofyanov.tm.model;

import java.util.UUID;

public final class Task {

    public String id = UUID.randomUUID().toString();
    public String name = "";
    public String description = "";

    public Task() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }
}
