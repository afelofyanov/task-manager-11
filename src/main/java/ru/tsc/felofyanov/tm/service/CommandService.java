package ru.tsc.felofyanov.tm.service;

import ru.tsc.felofyanov.tm.api.repository.ICommandRepository;
import ru.tsc.felofyanov.tm.api.service.ICommandService;
import ru.tsc.felofyanov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }
}
