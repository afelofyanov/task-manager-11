package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;

public interface ITaskService {
    Task add(Task task);

    List<Task> findAll();

    Task remove(Task task);

    Task create(String name);

    Task create(String name, String description);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    void clear();
}
