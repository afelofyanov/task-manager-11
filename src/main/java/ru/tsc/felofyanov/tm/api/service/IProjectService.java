package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.model.Project;

import java.util.List;

public interface IProjectService {
    Project add(Project project);

    List<Project> findAll();

    Project remove(Project project);

    Project create(String name);

    Project create(String name, String description);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project removeById(String id);

    Project removeByIndex(Integer index);
}
