package ru.tsc.felofyanov.tm.repository;

import ru.tsc.felofyanov.tm.api.repository.ICommandRepository;
import ru.tsc.felofyanov.tm.constant.ArgumentConst;
import ru.tsc.felofyanov.tm.constant.TerminalConst;
import ru.tsc.felofyanov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show system info."
    );

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show applicant version."
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show terminal commands info."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show command list."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show argument list."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show task list."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null,
            "Update task by index."
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null,
            "Show task by index."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null,
            "Remove task by id."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null,
            "Update task by id."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null,
            "Show task by id."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Show project list."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index."
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null,
            "Show project by index."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null,
            "Update project by id."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null,
            "Show project by id."
    );

    private final Command[] terminalCommands = new Command[]{
            INFO, ABOUT, VERSION, HELP,
            ARGUMENTS, COMMANDS,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            TASK_REMOVE_BY_INDEX, TASK_UPDATE_BY_INDEX, TASK_SHOW_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_UPDATE_BY_ID, TASK_SHOW_BY_ID,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            PROJECT_REMOVE_BY_INDEX, PROJECT_UPDATE_BY_INDEX, PROJECT_SHOW_BY_INDEX,
            PROJECT_REMOVE_BY_ID, PROJECT_UPDATE_BY_ID, PROJECT_SHOW_BY_ID,
            EXIT
    };

    @Override
    public Command[] getCommands() {
        return terminalCommands;
    }
}
